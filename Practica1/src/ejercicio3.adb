with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO;
with Ada.Float_Text_IO;
with pkg_ejercicio2c; use pkg_ejercicio2c;

procedure ejercicio3 is
   package diasSemana is new Enumeration_IO(pkg_ejercicio2c.TdiasSemana); use diasSemana;
   diaElegido : TdiasSemana;
begin
   Ada.Integer_Text_IO.Put(numAlumnos);
   New_Line;
   Ada.Float_Text_IO.Put(getNotaMedia, 1, 1, 0);
   New_Line;

   for dia in TdiasSemana loop
      Put(dia);
      New_Line;
   end loop;
   Put_Line("Introduce un d�a de la semana: ");
   Get(diaElegido);
   Put(diaElegido);
end ejercicio3;
