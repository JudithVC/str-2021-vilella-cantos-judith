with Ada.Text_IO; use Ada.Text_IO;
with pkg_ejercicio2c;
with Ada.Integer_Text_IO;

procedure ejercicio1 is
   s : String := "Comenzamos las pr�cticas de STR";
   mes : Natural;

begin
   begin
   Put("Hola Mundo!!! ");
   Put_Line(s);
   pkg_ejercicio2c.OtroProcedimiento;
   Put_Line("Introduce el n�mero del mes (1-12)");
   Ada.Integer_Text_IO.Get(mes);

   case mes is
    when 1 | 2 | 12 => Put_Line("Invierno");
    when 3..5 => Put_Line("Primavera");
    when 6..8 => Put_Line("Verano");
    when 9..11 => Put_Line("Oto�o");
    when others => Put_Line("Mes incorrecto");
   end case;
   exception
      when others => Put_Line("El n�mero de mes debe ser > 0");
   end;
   Put_Line("FIN DEL PROGRAMA");

end ejercicio1;
