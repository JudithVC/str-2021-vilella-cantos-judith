with Ada.Text_IO; use Ada.Text_IO;

package pkg_ejercicio2c is
   procedure OtroProcedimiento;
   function getNotaMedia return Float;

   type TdiasSemana is (Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo);
   numAlumnos : Integer := 7;
private
   notaMedia : Float := 3.19;

end pkg_ejercicio2c;
