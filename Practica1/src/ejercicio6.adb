with Ada.Numerics.Float_Random;  -- paquete gen�rico predefinido
with Ada.Float_Text_IO; USE Ada.Float_Text_IO;
with Ada.Text_IO; use Ada.Text_IO;
procedure ejercicio6 is
   subtype T_Digito is Float range 0.0 .. 1.0;
   -- crear instancia del paquete gen�rico predefinido
   Generador_Digito : Ada.Numerics.Float_Random.Generator;
   digito                   : T_Digito;
begin
   Ada.Numerics.Float_Random.Reset (Generador_Digito); -- Inicializa generador n�meros aleatorios
   loop
      digito := Ada.Numerics.Float_Random.Random(generador_digito); -- generar n�mero aleatorio
      Put(digito, 1, 4, 0);
      skip_line;
   end loop;
end ejercicio6;
