with PKG_graficos;
with PKG_Averias;
with pkg_tipos; use pkg_tipos;
with Ada.Real_Time; use Ada.Real_Time;

package body pkg_teleoperador_mecanico is

   task body teleoperador is
      estado : pkg_tipos.T_Estado_Averia;
      pinchazo : PKG_tipos.T_Tipo_Averia := PKG_tipos.PINCHAZO;
      --sinGasolina : PKG_tipos.T_
   begin
      loop
         select
            accept repararAveria (tipoAveria : in PKG_tipos.T_Tipo_Averia; carril : in PKG_tipos.T_Carril) do
               estado := PKG_tipos.COMUNICADA_AVERIA;
               PKG_graficos.Actualiza_Tipo_Averia_Carril(carril, tipoAveria);
               PKG_Averias.OP_Indicador_Averia(carril).Actualiza_Indicador(estado);
               if tipoAveria = SIN_GASOLINA then
                     requeue mecanicoGasolina.empezar_reparacion;
                  else
                     requeue mecanicoPinchazo.empezar_reparacion;
                  end if;  
            end repararAveria;
         or
            accept finAveria (carril : in PKG_tipos.T_Carril) do
               estado := PKG_tipos.NO_AVERIA;
               PKG_graficos.Actualiza_Reloj_Averia(carril, 0);
               PKG_Averias.OP_Indicador_Averia(carril).Actualiza_Indicador(estado);
            end finAveria;
         end select;
      end loop;
   end teleoperador;
   
   task body mecanico is
      estado : pkg_tipos.T_Estado_Averia := PKG_tipos.REPARANDO_AVERIA;
      duracionAveria : Integer;
      Siguiente : Time;
      periodo : Time_Span := To_Time_Span(1.0);
   begin
      loop
         accept empezar_reparacion(tipoAveria : in PKG_tipos.T_Tipo_Averia;carril  : in PKG_tipos.T_Carril) do
            PKG_Averias.OP_Indicador_Averia(carril).Actualiza_Indicador(estado);
            duracionAveria := Integer(PKG_Averias.Duracion_Tipo_Averia(tipoAveria));
            Siguiente := Clock + periodo;
            loop
               PKG_graficos.Actualiza_Reloj_Averia(carril, duracionAveria);              
               delay until (Siguiente);
               duracionAveria := duracionAveria - 1;
               Siguiente := Siguiente + periodo;
               exit when duracionAveria = 0;
            end loop;
            teleoperador.finAveria(carril);
         end empezar_reparacion;
      end loop; 
   end mecanico;
   
end pkg_teleoperador_mecanico;

   
