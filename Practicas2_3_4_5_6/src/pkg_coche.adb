with PKG_graficos;
with PKG_debug;
with Ada.exceptions; use Ada.exceptions;
WITH Ada.Numerics.Discrete_Random;
with PKG_Semaforo;
with PKG_Averias;
with pkg_teleoperador_mecanico;

package body pkg_coche is
   
   task body Tarea_GeneraCoches is
      tarea_coche : Ptr_Tarea_Coche;
      ptr_coche : Ptr_T_RecordCoche;
      id_coche : T_IdCoche;
      id : Integer;
      
      package Pkg_retardo_Coche_aleatorio is new Ada.Numerics.Discrete_Random(T_RetardoCoches);
      generador_Retardo : Pkg_retardo_Coche_aleatorio.Generator;
      retardoCoche : T_RetardoCoches;
      
      package pkg_color_coche is new Ada.Numerics.Discrete_Random(T_ColorCoche);
      generador_Color : pkg_color_coche.Generator;
      colorCoche : T_ColorCoche;
      
      package pkg_velocidad_aleatoria is new Ada.Numerics.Discrete_Random(T_RangoVelocidadAparicion);
      generador_velocidad : pkg_velocidad_aleatoria.Generator;
      velocidad_coche_inicial : T_RangoVelocidadAparicion;
      
      package pkg_carril_aleatorio is new Ada.Numerics.Discrete_Random(T_Carril);
      generador_carril : pkg_carril_aleatorio.Generator;
      
   begin
      PKG_debug.Escribir("======================INICIO TASK GeneraCoches");
      id := 1;
      Pkg_retardo_Coche_aleatorio.Reset(generador_Retardo);
      pkg_color_coche.Reset(generador_Color);
      pkg_velocidad_aleatoria.Reset(generador_velocidad);
      pkg_carril_aleatorio.Reset(generador_carril);
      LOOP
         select
            PKG_Averias.OP_Indicador_Averia(carril).comunicaAveria;
            PKG_graficos.Dibujar_Carril_Cerrado(carril);
         then abort
            loop
               retardoCoche := Pkg_retardo_Coche_aleatorio.Random(generador_Retardo);
               delay(Duration(retardoCoche));
               ptr_coche := new T_RecordCoche;
               id_coche := T_IdCoche(id);
               PKG_graficos.Actualiza_Atributo(id_coche, ptr_coche.all);
               colorCoche := pkg_color_coche.Random(generador_Color);
               PKG_graficos.Actualiza_Atributo(colorCoche, ptr_coche.all);
               velocidad_coche_inicial := pkg_velocidad_aleatoria.Random(generador_velocidad);
               pkg_graficos.Actualiza_Atributo(velocidad_coche_inicial, ptr_coche.all);
               PKG_graficos.Actualiza_Atributo(carril, ptr_coche.all);
               PKG_graficos.Actualiza_Atributo(PKG_graficos.Pos_Inicio(ptr_coche.all), ptr_coche.all);
               tarea_coche := new T_Tarea_Coche(ptr_coche);
               id := id + 1;
            end loop;
         end select;
         PKG_Averias.OP_Indicador_Averia(carril).comunicaReparacion;
         PKG_graficos.Dibujar_Carril_Abierto(carril);
       --  IF PKG_Averias.OP_Indicador_Averia(carril).Consulta_Indicador = COMUNICADA_AVERIA OR PKG_Averias.OP_Indicador_Averia(carril).Consulta_Indicador = REPARANDO_AVERIA then
           -- PKG_graficos.Dibujar_Carril_Cerrado(carril);
           -- exit;
            --PKG_graficos.Dibujar_Carril_Abierto(carril);
         --end if;
      end loop;
      exception
       when event: others =>
        PKG_debug.Escribir("ERROR en TASK GeneraCoches1: " & Exception_Name(Exception_Identity(event)));
   end Tarea_GeneraCoches;
   
   task body T_Tarea_Coche is
      coche : T_RecordCoche;
      package pkg_velocidad_aleatoria is new Ada.Numerics.Discrete_Random(T_RangoVelocidad);
      generador_velocidad : pkg_velocidad_aleatoria.Generator;
      velocidad : T_RangoVelocidad;
      package pkg_tipoAveria_aleatorio is new Ada.Numerics.Discrete_Random(T_Tipo_Averia);
      generador_tipoAveria : pkg_tipoAveria_aleatorio.Generator;
      tipoAveria : T_Tipo_Averia;
      velocidad_aux : T_RangoVelocidad;
   begin
      PKG_debug.Escribir("======================INICIO TASK Coche");
      coche := ptr_coche.all;
      PKG_graficos.Aparece(coche);
      while not PKG_graficos.Pos_Final(coche) loop 
         if PKG_graficos.Tiene_Averia(coche) then
            if PKG_graficos.Coche_Parado(coche) = False then
               velocidad_aux := coche.velocidad.X;
            else
               velocidad_aux := pkg_velocidad_aleatoria.Random(generador_velocidad);
            end if;
            
            velocidad := 0;
            PKG_graficos.Actualiza_Atributo(velocidad, coche);
            PKG_graficos.Actualiza_Movimiento(coche);
            tipoAveria := pkg_tipoAveria_aleatorio.Random(generador_tipoAveria);
            pkg_teleoperador_mecanico.teleoperador.repararAveria(tipoAveria, coche.Carril);
            --delay(5.0);
            --pkg_teleoperador.teleoperador.finAveria(coche.Carril);
            --PKG_Averias.OP_Indicador_Averia(coche.Carril).Actualiza_Indicador(NO_AVERIA);
            -- velocidad := VELOCIDAD_COCHE;
            PKG_graficos.Actualiza_Atributo(velocidad_aux, coche);
            PKG_graficos.Actualiza_Movimiento(coche);
        end if;
        if pkg_graficos.Posicion_Stop_Semaforo(coche) and PKG_Semaforo.OP_Semaforo.Semaforo_Rojo then 
           velocidad := 0;
           PKG_graficos.Actualiza_Atributo(velocidad, coche);
           PKG_graficos.Actualiza_Movimiento(coche);
           PKG_Semaforo.OP_Semaforo.esperarSemaforo;
           velocidad := pkg_velocidad_aleatoria.Random(generador_velocidad);
           PKG_graficos.Actualiza_Atributo(velocidad, coche);
           PKG_graficos.Actualiza_Movimiento(coche);
        else
           if PKG_graficos.Coche_Parado(coche) then
               PKG_Semaforo.OP_Semaforo.esperarSemaforo;
               velocidad := pkg_velocidad_aleatoria.Random(generador_velocidad);
               PKG_graficos.Actualiza_Atributo(velocidad, coche);
           end if;
         end if;
         PKG_graficos.Actualiza_Movimiento(coche);
         delay(RETARDO_MOVIMIENTO);
      end loop;
      PKG_graficos.Desaparece(coche);
   exception
      when PKG_tipos.DETECTADA_COLISION_TREN =>
         PKG_debug.Escribir("ERROR en TASK Coche: DETECTADA_COLISION_TREN");
         PKG_graficos.Desaparece(coche);
       when event: others =>
        PKG_debug.Escribir("ERROR en TASK Coche: " & Exception_Name(Exception_Identity(event)));
   end T_Tarea_Coche;
   
end pkg_coche;
