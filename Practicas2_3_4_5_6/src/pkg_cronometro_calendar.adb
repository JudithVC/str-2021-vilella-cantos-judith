with PKG_graficos;
with Ada.Calendar; use Ada.Calendar;

package body pkg_cronometro_calendar is

   task body cronometro is
      periodo : constant Duration := 1.0;
      Inicio : Time;
      Fin : Time;
      Siguiente :Time;
      Ahora : Duration;
   begin
      Inicio := Clock;
      Siguiente := Clock + periodo;
      LOOP
         Fin := Clock;
         Ahora := Fin - Inicio;
         PKG_graficos.Actualiza_Cronometro(Ahora);
         --delay periodo;
         delay until Siguiente;
         Siguiente := Siguiente + periodo;
      end loop;
   end cronometro;
   

end pkg_cronometro_calendar;
