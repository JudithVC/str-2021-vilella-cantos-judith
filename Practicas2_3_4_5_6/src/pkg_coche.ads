with PKG_tipos; use PKG_tipos;
package pkg_coche is

   --task T_GeneraCochesGeneral;
   TASK TYPE Tarea_GeneraCoches(carril: T_Carril);
   Tarea_GeneraCoches1 : Tarea_GeneraCoches(1);
   Tarea_GeneraCoches2 : Tarea_GeneraCoches(2);
   --type ptr_genera_coches is access Tarea_GeneraCoches;

   TASK TYPE T_Tarea_Coche(ptr_coche : Ptr_T_RecordCoche);
   type Ptr_Tarea_Coche is access T_Tarea_Coche;

end pkg_coche;
