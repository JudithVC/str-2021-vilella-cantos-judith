with PKG_tipos; use PKG_tipos;

package pkg_tren is

   TASK tarea_GeneraTrenes;
   
   TASK type T_Tarea_Tren(ptr_tren: Ptr_T_RecordTren);
   type Ptr_Tarea_Tren is access T_Tarea_Tren;

end pkg_tren;
