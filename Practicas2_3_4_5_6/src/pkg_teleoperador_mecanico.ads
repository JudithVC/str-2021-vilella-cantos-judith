with pkg_tipos;

package pkg_teleoperador_mecanico is

   task teleoperador is
      entry repararAveria(tipoAveria : in PKG_tipos.T_Tipo_Averia;carril  : in PKG_tipos.T_Carril);
      entry finAveria(carril  : in PKG_tipos.T_Carril);
   end teleoperador;
   
   task type mecanico is
      entry empezar_reparacion(tipoAveria : in PKG_tipos.T_Tipo_Averia; carril  : in PKG_tipos.T_Carril);
   end mecanico;
   mecanicoPinchazo : mecanico;
   mecanicoGasolina : mecanico;
   
end pkg_teleoperador_mecanico;
