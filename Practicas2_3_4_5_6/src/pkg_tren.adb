with PKG_graficos;
with PKG_debug;
with Ada.exceptions; use Ada.exceptions;
WITH Ada.Numerics.Discrete_Random;
with Ada.Calendar; use Ada.Calendar;
with PKG_Semaforo;

package body pkg_tren is

   task body tarea_GeneraTrenes is
      tarea_tren : Ptr_Tarea_Tren;
      ptr_tren : Ptr_T_RecordTren;
      PACKAGE Pkg_NumVagones_Aleatorio
                is new Ada.Numerics.Discrete_Random(T_NumVagones);
      Generador_NumVagones: Pkg_NumVagones_Aleatorio.Generator;
      numVagones   : T_NumVagones;

      -- Creaci�n de paquete para generar un color aleatorio
      PACKAGE Pkg_ColorTren_Aleatorio
                is new Ada.Numerics.Discrete_Random(T_ColorTren);
      Generador_ColorTren: Pkg_ColorTren_Aleatorio.Generator;
      colorTren   : T_ColorTren;
      Siguiente : Time;
   BEGIN
      PKG_debug.Escribir("======================INICIO TASK GeneraTrenes");
      Pkg_NumVagones_Aleatorio.Reset(Generador_NumVagones);
      Pkg_ColorTren_Aleatorio.Reset(Generador_ColorTren);
      Siguiente := Clock + PERIODO_TREN;
      LOOP
         if not PKG_graficos.Hay_Tren then
            ptr_tren := new T_RecordTren;
            numVagones := Pkg_NumVagones_Aleatorio.Random(Generador_NumVagones);
            colorTren := Pkg_ColorTren_Aleatorio.Random(Generador_ColorTren);
            
            PKG_graficos.Actualiza_Atributo(numVagones, ptr_tren.all);
            PKG_graficos.Actualiza_Atributo(colorTren, ptr_tren.all);
            tarea_tren := new T_Tarea_Tren(ptr_tren);
         end if;
         DELAY until Siguiente;
         Siguiente := Siguiente + PERIODO_TREN;
      end loop;
       exception
       when event: others =>
        PKG_debug.Escribir("ERROR en TASK GeneraTrenes: " & Exception_Name(Exception_Identity(event)));

   END tarea_GeneraTrenes;
   
   
   
   task body T_Tarea_Tren is
      tren : T_RecordTren;
      parado_trafico : Boolean := false;
   begin
      PKG_debug.Escribir("======================INICIO TASK Tren");
      tren := ptr_tren.all;
      pkg_graficos.Aparece(tren);
      while not PKG_graficos.Pos_Final(tren) loop
         IF PKG_graficos.Dentro_Zona_Cruce(Tren) and not parado_trafico THEN
            PKG_Semaforo.OP_Semaforo.Cambia_Estado_Semaforo(Red);
            Parado_Trafico := True;
         ELSIF NOT PKG_graficos.Dentro_Zona_Cruce(Tren) AND Parado_Trafico THEN
            PKG_Semaforo.OP_Semaforo.Cambia_Estado_Semaforo(Green);
            Parado_Trafico := False;
         END IF;
         PKG_graficos.Actualiza_Movimiento(tren);
         delay(RETARDO_MOVIMIENTO);
      end loop;
      -- El tren deja de visualizarse
      PKG_graficos.Desaparece(tren);

      PKG_debug.Escribir("======================FIN TASK Tren");

      exception
       when event: others =>
        PKG_debug.Escribir("ERROR en TASK Tren: " & Exception_Name(Exception_Identity(event)));
   end T_Tarea_Tren;
   
end pkg_tren;
