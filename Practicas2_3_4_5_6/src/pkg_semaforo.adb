--******************* PKG_SEMAFORO.ADB ************************************
-- Paquete que implementa el funcionamiento del sem�foro para cruzar la v�a del tren
--***********************************************************************

WITH pkg_graficos;
with PKG_tipos; use pkg_tipos;

PACKAGE BODY Pkg_Semaforo IS

   ------------------------------------------------------------------------
   -- OBJETO PROTEGIDO PARA ALMACENAR EL ESTADO DEL SEMAFORO
   ------------------------------------------------------------------------
   PROTECTED BODY OP_Semaforo IS

      ---------------------------------------------------------------------
      -- Cambia el estado (color) del sem�foro de coches
      ---------------------------------------------------------------------
      PROCEDURE Cambia_Estado_Semaforo(color: IN T_ColorSemaforo) IS
      BEGIN
         -- cambiamos el estado almacenado en el OP
         Estado_Semaforo := Color;

         -- Dibujamos el nuevo color en la interfaz gr�fica
         pkg_graficos.Actualiza_Color_Semaforo(color);
      END Cambia_Estado_Semaforo;


      ---------------------------------------------------------------------
      -- Funci�n que devuelve true si el sem�foro est� en rojo
      ---------------------------------------------------------------------
      FUNCTION Semaforo_Rojo RETURN Boolean IS
      BEGIN
         RETURN Estado_Semaforo = RED;
      END Semaforo_Rojo;

      ENTRY esperarSemaforo WHEN Semaforo_Rojo = False IS
      begin
         null;
      end esperarSemaforo;


   end OP_Semaforo;

   protected body OP_indTaxi is


      procedure taxiSePara is
      begin
         taxi_en_parada := True;
      end taxiSePara;

      procedure taxiSeVa is
      begin
         taxi_en_parada := False;
      end taxiSeVa;

      procedure paradaVacia is
      begin
         plazas_ocupadas := 0;
      end paradaVacia;


      function plazas return Integer is
      begin
         return plazas_ocupadas;
      end plazas;


      function taxiEnParada return Boolean is
      begin
         return taxi_en_parada;
      end taxiEnParada;


      entry esperarParada WHEN taxi_en_parada = True AND plazas_ocupadas < CAPACIDAD_TAXI  is
      begin
         plazas_ocupadas := plazas_ocupadas + 1;
      end esperarParada;


      entry esperarLlenarse WHEN plazas_ocupadas = CAPACIDAD_TAXI  is
      begin
         null;
      end esperarLlenarse;


   end OP_indTaxi;


END PKG_semaforo;
