with Ada.Real_Time; use Ada.Real_Time;
with PKG_graficos;

package body pkg_cronometro_realtime is

   task body cronometro_realtime is
      periodo : constant Time_Span := To_Time_Span(1.0);
      Inicio : Time;
      Fin : Time;
      Ahora : Time_Span;
      Siguiente : Time;
   begin
      Inicio := Clock;
      Siguiente := Clock + periodo;
      LOOP
         Fin := Clock;
         Ahora := Fin - Inicio;
         PKG_graficos.Actualiza_Cronometro(To_Duration(Ahora));
         delay until Siguiente;
         Siguiente := Siguiente + periodo;
      end loop;
      
   end cronometro_realtime;

end pkg_cronometro_realtime;
